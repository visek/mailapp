package org.visapps.mailapp.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

public class DatabaseConfig {


    public DataSource ds(){
        HikariConfig config = new HikariConfig();
        config.setDataSourceClassName("");
        return new HikariDataSource(config);
    }
}
