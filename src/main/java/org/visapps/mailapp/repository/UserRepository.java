package org.visapps.mailapp.repository;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.visapps.mailapp.models.User;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepository {



    public List<User> getAllusers(){

        HikariConfig config = new HikariConfig();
        config.setDataSourceClassName("");
        DataSource ds = new HikariDataSource(config);

        List<User> users = new ArrayList<>();

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rs = null;

        try{
            connection = ds.getConnection();
            statement = connection.prepareStatement("select * from users");
            rs = statement.executeQuery();
            while(rs.next()){
                Long id = rs.getLong("id");
                String name = rs.getString("name");
                User user = new User(id, name);
                users.add(user);
            }
            connection.close();
        }
        catch(Exception e){

        }
        return users;
    }
}
