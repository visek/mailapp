package org.visapps.mailapp.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile("prod")
public class Info {

    @Value("${controller.common.version}")
    private String version;

    @RequestMapping("/info/")
    public String info() {
        return version;
    }
}
