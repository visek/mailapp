package org.visapps.mailapp.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class Hello {


    @Value("${controller.common.hello}")
    private String hello;

    @RequestMapping("/")
    public String home() {
        return hello;
    }
}
